import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

public class Highway extends JLayeredPane implements ActionListener{
	private String bgPath = "./pic/background.png";
	private JButton startingPointAddCar;
	private JButton intersectionAddCar;
	

	public static final int startingPoint = 0;
	public static final int intersection = 15;
	public static final int highwayLength = 30;
	public static final int highwayAbsoluteY = 300;
	public static final int speedLimit = 4;

	Car [] road;

	public Highway(){
		setLayout(null); 
	    try{
	      ImageIcon background = new ImageIcon(this.getClass().getResource(bgPath));
	      JLabel l = new JLabel(background);
	      l.setBounds(0, 0, 960, 512);
	      add(l, JLayeredPane.DEFAULT_LAYER);
	    }catch(Exception e){
	      System.out.println(e);
	    }

		
	    startingPointAddCar = new JButton("Add Car");
	    startingPointAddCar.setBounds(0, 400, 100, 20);
	    startingPointAddCar.setActionCommand("startingPoint");
	    startingPointAddCar.addActionListener(this);
	    add(startingPointAddCar, JLayeredPane.PALETTE_LAYER);

	    intersectionAddCar = new JButton("Add Car");
	    intersectionAddCar.setBounds(500, 400, 100, 20);
	    intersectionAddCar.setActionCommand("intersection");
	    intersectionAddCar.addActionListener(this);
	    add(intersectionAddCar, JLayeredPane.PALETTE_LAYER);

	    road = new Car[highwayLength];	


	}

	public synchronized void drawBoom(BufferedImage boomImage, int boomPosition){
		Graphics g = getGraphics();
		super.paint(g);
        Graphics2D g2d = (Graphics2D)g;
    	g2d.drawImage(boomImage, boomPosition*32-62+16, highwayAbsoluteY-62+16, 128, 128, this);
    	//Toolkit.getDefaultToolkit().sync();
      	g.dispose();
        
	}

	public synchronized void boomIntoAshes(Car c){
		road[c.getXPosition()] = null;
		c.setXPosition(highwayLength);
		synchronized(c){
			c.notify();
		}

	}


	public synchronized void moveCar(Car c) throws RuntimeException{
		int thisPosition = c.getXPosition();
		int frontCarPosition = thisPosition+1;

		while(frontCarPosition < highwayLength &&  road[frontCarPosition] == null)frontCarPosition++;

		int nextVelocity;
		if(frontCarPosition >= highwayLength)nextVelocity = speedLimit;
		else {
			nextVelocity = ((frontCarPosition - thisPosition)/2 > speedLimit? speedLimit : (frontCarPosition - thisPosition)/2);
			nextVelocity = (nextVelocity < 1 ? 1 : nextVelocity);
			if(thisPosition+nextVelocity >= frontCarPosition && thisPosition+nextVelocity < highwayLength){
				
				//road[thisPosition].boom(thisPosition);
				road[frontCarPosition].boom(frontCarPosition);
				remove(c.getCarJLabel());
				if(road[thisPosition] == c)road[thisPosition] = null;
				c.setXPosition(highwayLength);
				synchronized(c){c.notify();}
				return;
				
				


			}
		}

		if(road[thisPosition] == c)road[thisPosition] = null;
		

		if(thisPosition+ nextVelocity < highwayLength)road[thisPosition+nextVelocity] = c;
		
		c.setXPosition(c.getXPosition()+nextVelocity);
		repaint();
		
		synchronized(c){
      		c.notify();
    	}
		
		

	}

	  public void actionPerformed(ActionEvent e) {
	  	String command = e.getActionCommand();
	  	if(command.equals("startingPoint")){	
  			road[startingPoint] = new Car(this, startingPoint);
  			road[startingPoint].start();

	  	}else if(command.equals("intersection")){
	  		road[intersection] = new Car(this, intersection);
	  		road[intersection].start();

	  	}


	  }

}
