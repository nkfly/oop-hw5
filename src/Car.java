import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;

public class Car extends Thread implements ActionListener{
	private static String boomPath = "./pic/boom.png";
	private static BufferedImage boomIn;
	private static String carImagePath = "./pic/car.png";
	private static BufferedImage carIn;

	static {
		try {
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();


			boomIn = ImageIO.read(classLoader.getResourceAsStream(boomPath));
//			File carImage = new File(carImagePath);
			carIn = ImageIO.read(classLoader.getResourceAsStream(carImagePath));
		}
		catch(Exception e){
			System.err.println(e);
		}


	}


	private Highway highway;
	private JLabel carJLabel;
	private int xPosition;
	private javax.swing.Timer timer;
	private int boomState = 0;
	private int boomPosition;

	public Car(Highway h, int position){
		highway = h;
		xPosition = position;
		


		carJLabel = new JLabel(new ImageIcon(carIn.getSubimage( 0, 96*2,126,96).getScaledInstance(32, 32, java.awt.Image.SCALE_SMOOTH) ));
		carJLabel.setBounds(position*32, highway.highwayAbsoluteY, 32, 32);

		timer = new javax.swing.Timer(1000, this);
		timer.setActionCommand("move");
		
	}

	JLabel getCarJLabel(){
		return carJLabel;
	}

	public void actionPerformed(ActionEvent e) {
		String command = e.getActionCommand();
		if(command.equals("move")){
			highway.moveCar(this);
			if(xPosition >= highway.highwayLength)timer.stop();

		}else if(command.equals("boom")){
			highway.drawBoom(boomIn.getSubimage(boomState%5*192, boomState/5%3*192, 192, 192), boomPosition);
			boomState++;
			if(boomState >= 29){
				timer.stop();
				highway.boomIntoAshes(this);

			}



		}
		
		


	}



	public void boom(int accidentPosition){
		boomPosition = accidentPosition;

		timer.stop();
		timer.setDelay(70);
		timer.setActionCommand("boom");
		timer.start();


		
	}

	

	public void run(){
		timer.start();
		highway.add(getCarJLabel(), JLayeredPane.PALETTE_LAYER);
		highway.repaint();
		while(xPosition < highway.highwayLength){
			synchronized(this){
		    try{
		      this.wait();
		    } catch(InterruptedException e){
		      System.err.println(e);
		    }
    		}
		}

		 highway.remove(getCarJLabel());
		 highway.repaint();
		 

	}

	public int getXPosition(){
		return xPosition;
	}
	public void setXPosition(int nextPosition){

		xPosition = nextPosition;
		carJLabel.setBounds(xPosition*32, highway.highwayAbsoluteY, 32, 32);

	}




	


}
