import java.util.*;
import java.util.concurrent.*;
import java.io.*;
import java.lang.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.imageio.ImageIO;



 
/* 
This HTML can be used to launch the applet: 
 
<object code="MyApplet" width=240 height=100> 
</object> 
 
*/ 
 
public class CarSimulation extends JApplet { 
  Highway highway;

 
  public void init() {
  	guiInit(); // initialize the GUI 
    try { 
      SwingUtilities.invokeAndWait(new Runnable () { 
        public void run() { 

          //guiInit(); // initialize the GUI 
        } 
      }); 
    } catch(Exception exc) { 
      System.out.println("Can't create because of "+ exc); 
    } 
  } 
 
  // Called second, after init().  Also called 
  // whenever the applet is restarted.  
  public void start() { 
    // Not used by this applet. 
  } 
 
  // Called when the applet is stopped. 
  public void stop() { 
    // Not used by this applet. 
  } 
 
  // Called when applet is terminated.  This is 
  // the last method executed. 
  public void destroy() { 
    // Not used by this applet. 
  } 
 
  // Setup and initialize the GUI.  
  private void guiInit() { 
    // Set the applet to use flow layout. 
    //setLayout(null);
    highway = new Highway();
    getContentPane().add(highway);



  } 
}




